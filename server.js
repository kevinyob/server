const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const port = 3002;
const api = require('./routes/api');
const app = express();
app.use(cors())
app.use(bodyParser.json()); 
app.use('/api', api);
app.get('/', (req, res) => {
      res.send('Hello Kevin')
})
 app.listen(port, function(){
        console.log("Server running on localhost:" + port);
})