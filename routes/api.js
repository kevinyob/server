const express = require('express');
const router = express.Router();
const jwt= require('jsonwebtoken');
const mongoose = require('mongoose');
const User = require('../models/user');
const db = "mongodb+srv://userkevin:kevin@cluster0-dakt2.mongodb.net/test?retryWrites=true";
mongoose.connect(db, function(err){
    if(err){
        console.error('Error! ' + err)
    } else {
      console.log('Connected to mongodb')      
    }
});
router.get('/', (req,res) => {
    res.send('From API')
})
function verifyToken(req, res, next) {
  if(!req.headers.authorization) {
    return res.status(401).send('Unauthorized request')
  }
  let token = req.headers.authorization.split(' ')[1]
  if(token === 'null') {
    return res.status(401).send('Unauthorized request')    
  }
  let payload = jwt.verify(token, '12345678')
  if(!payload) {
    return res.status(401).send('Unauthorized request')    
  }
  req.userId = payload.subject
  next()
}

router.post('/register', (req, res) => {
    let userData = req.body
    let user = new User(userData)
    user.save((err, registeredUser) => {
      if (err) {
        console.log(err)      
      } else {
        let payload = {subject: registeredUser._id}
      let token = jwt.sign(payload, '12345678')
      res.status(200).send({token})
        // res.status(200).send({registeredUser})
      }
    })
  })

  router.post('/login', (req, res) => {
    let userData = req.body
    User.findOne({email: userData.email,password:userData.password}, (err, user) => {
      if (err) {
        console.log(err)    
      } else {
        if (!user) {
          res.status(401).send('Invalid Email')
        } else 
        if ( user.password !== userData.password) {
          res.status(401).send('Invalid Password')
        } else {
          let payload = {subject: user._id}
        let token = jwt.sign(payload, '12345678')
        res.status(200).send({token})
        }
      }
    })
  })
  router.get('/events',verifyToken ,(req,res) => {
  
  })

module.exports = router